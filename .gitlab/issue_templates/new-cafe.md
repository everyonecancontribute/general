<!-- 
Set the title: 🇺🇸 #everyonecancontribute cafe | #<number> - 2020-MM-DD
-->

# Kaeffchen Agenda

<!-- Format: 
- @<username>: <topic> [<urls>]
  - More details 
-->

**Time: 18:00 CET / 9am PT**

## Community

- [Apply Form](https://forms.gle/1Km1emjpZvoUGTQh8)
- [Review Idea Pitches](https://gitlab.com/everyonecancontribute/general/-/issues?label_name%5B%5D=topic%3A%3Aidea)

## News

Interesting articles, best practices, etc. for everyone to read and learn async:

### Bookmarks

## Topics

The main theme for this week is: 

### CI/CD

### DevSecOps

### Social

<!-- Actions, do not edit -->

/label ~"topic::meeting" ~"lang::en"

/cc @everyonecancontribute 
/assign @dnsmichi

