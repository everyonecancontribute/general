# Organisation

This project is used for organisation issues and project management plans.

- [Global issue list](https://gitlab.com/groups/everyonecancontribute/-/issues)

## Create new Issues

Use Case		| Recommendation
------------------------|------------------------
Pitch an idea           | [Create a new Idea issue](https://gitlab.com/everyonecancontribute/general/-/issues/new?issuable_template=new-idea) in this general project.
🇺🇸 Cafe agenda          | [Create a new Cafe issue](https://gitlab.com/everyonecancontribute/general/-/issues/new?issuable_template=new-cafe) in this general project.
🇩🇪 Kaeffchen agenda     | [Create a new Kaeffchen issue](https://gitlab.com/everyonecancontribute/general/-/issues/new?issuable_template=new-kaeffchen) in this general project.
Add a blog post (idea)	| [New issue](https://gitlab.com/everyonecancontribute/everyonecancontribute.gitlab.io/-/issues/new) in the website project.
CI/CD changes for pages | [New issue](https://gitlab.com/everyonecancontribute/everyonecancontribute.gitlab.io/-/issues/new) in the website project.


## Issue/MR Label Workflow

[Labels](https://gitlab.com/groups/everyonecancontribute/-/labels) are managed on the group level with prefixed scopes.

Label name		| Description
------------------------|------------------------
`action::<type>`	| Describes the required action, for example feedback on issues or reviews of blog posts.
`area::<name>`		| Specifies the target area for this issue/MR: Website, infrastructure with CI/CD deployments, blog specifically, etc.
`lang::*`		| Language marker when needed. German Kaeffchen will always require `lang::de` for example.
`topic::<name>`		| Defines the topic for the issue/MR: Blog post, documentation or a meeting (Kaeffchen and more).

# Process for Weekly Meetings

## Pre Tasks

- Check for new members in the
  - 🇺🇸 Cafe [apply form](https://docs.google.com/forms/d/1cY1_uh-2zo4p0cySDU-iQF7o6naQMigz5_bH7u__fgw/edit#responses) (Michael)
  - 🇩🇪 Kaeffchen [apply form](https://docs.google.com/forms/d/1DqtUwD2R8QNBxOM1PFQE6MWBo9pJKheY9jiHH3ysolU/edit#responses) (Michael)
- Update the [agenda](https://gitlab.com/everyonecancontribute/general/-/issues?label_name%5B%5D=topic%3A%3Ameeting)
- Promote on social

## Start Tasks

- Start the Zoom stream to GitLab Unfiltered on YouTube (Michael)

## Post Tasks

- Create a new agenda issue for next week
  - 🇺🇸 Cafe [agenda](https://gitlab.com/everyonecancontribute/general/-/issues/new?issuable_template=new-cafe) 
  - 🇩🇪 Kaeffchen [agenda](https://gitlab.com/everyonecancontribute/general/-/issues/new?issuable_template=new-kaeffchen)
- Write a summary blog post based on the current agenda markdown content
  - Copy the YouTube URLs and Tweet insights
  - Review & merge the blog post. The `main` branch automatically deploys to https://everyonecancontribute.com/ 


### YouTube

YouTube playlists are linked on https://everyonecancontribute.com/ 

#### 🇺🇸 Cafe 

Title template:

```
XX. #everyonecancontribute cafe: TOPIC

Blog: https://everyonecancontribute.com/

Mentioned URLs:

```

#### 🇩🇪 Kaeffchen 

Same as 🇺🇸 Cafe with an updated title. 


### Blog Post

#### 🇺🇸 Cafe

```
---
Title: "XX. Cafe: FIXME"
Date: 2021-XX-XX FIXME
Aliases: []
Tags: ["cloud","FIXME"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights


### Insights

- ...

### Recording

Enjoy the session! 🦊 

{{< youtube FIXME >}}

```

#### 🇩🇪 Kaeffchen 

Same as 🇺🇸 Cafe with an updated title. 


