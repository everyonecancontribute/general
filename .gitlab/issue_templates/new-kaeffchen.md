<!-- 
Set the title: 🇩🇪 #everyonecancontribute Kaeffchen | #<number> - 2020-MM-DD
-->

# Kaeffchen Agenda

<!-- Format: 
- @<username>: <topic> [<urls>]
  - More details 
-->

**Time: 16:00 CEST**

## Community

- [Apply Form](https://forms.gle/LiK1ANVbfNSt89hW9)
- [Review Idea Pitches](https://gitlab.com/everyonecancontribute/general/-/issues?label_name%5B%5D=topic%3A%3Aidea)

### everyonecancontribute.com Updates

## News

Interesting articles, best practices, etc. for everyone to read and learn async:

### Bookmarks

## Topics

The main theme for this week is: 

### CI/CD

### DevSecOps

### Social

<!-- Actions, do not edit -->

/label ~"topic::meeting" ~"lang::de"

/cc @everyonecancontribute 
/assign @dnsmichi
