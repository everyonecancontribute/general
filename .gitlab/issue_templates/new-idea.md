<!-- 
Set the title: Idea: <your topic>, YYYY-MM-DD

Example: Idea: Grafana and Jsonnet, 2021-04-28
-->

# 👋 Idea

<!-- Please describe your idea in detail. -->

## 📅 Proposed Date

<!-- Please link the date from the events calendar at https://everyonecancontribute.com/page/events/ -->

- [ ] YYYY-MM-DD 

## ☕ Area

Pick all which apply.

- [ ] DevSecOps
- [ ] Cloud Native
- [ ] CI/CD
- [ ] Social
- [ ] Misc

## 🏗 Description

<!--
How did you learn more about the topic?
How can others benefit from your story?
Which key points would you want to discuss/highlight?
-->

## ✍ Relevant URLs

- [Website]()
- [Docs]()

## 💻 Resources

Can you prepare slides, live demo, a short story or a blog post? Pick all that apply.

- [ ] Presentation/Slides
- [ ] Live demo
- [ ] Blog post
- [ ] Story 
- [ ] Others: Please specify


<!-- Actions - DO NOT EDIT -->
/label ~"topic::idea" 

/assign @dnsmichi 

/cc @everyonecancontribute 


